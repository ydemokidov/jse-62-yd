package com.t1.yd.tm.repository.dto;

import com.t1.yd.tm.dto.model.AbstractUserOwnedEntityDTO;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface AbstractDtoUserOwnedJpaRepository<E extends AbstractUserOwnedEntityDTO> extends AbstractDtoJpaRepository<E> {

    void deleteAllByUserId(String userId);

    void deleteByUserIdAndId(String userId, String id);

    boolean existsByUserIdAndId(String userId, String id);

    List<E> findAllByUserId(String userId);

    List<E> findAllByUserId(String userId, Sort sort);

    Optional<E> findByUserIdAndId(String userId, String id);

}
