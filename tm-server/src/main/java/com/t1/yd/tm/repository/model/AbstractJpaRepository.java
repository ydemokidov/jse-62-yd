package com.t1.yd.tm.repository.model;

import com.t1.yd.tm.model.AbstractEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface AbstractJpaRepository<E extends AbstractEntity> extends JpaRepository<E, String> {


}
