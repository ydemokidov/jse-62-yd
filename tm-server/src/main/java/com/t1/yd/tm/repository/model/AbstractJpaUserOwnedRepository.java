package com.t1.yd.tm.repository.model;

import com.t1.yd.tm.model.AbstractUserOwnedEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface AbstractJpaUserOwnedRepository<E extends AbstractUserOwnedEntity> extends AbstractJpaRepository<E> {

    void deleteAllByUserId(String userId);

    void deleteByUserIdAndId(String userId, String id);

    boolean existsByUserIdAndId(String userId, String id);

    List<E> findAllByUserId(String userId);

    List<E> findAllByUserId(String userId, Sort sort);

    Optional<E> findByUserIdAndId(String userId, String id);

}
