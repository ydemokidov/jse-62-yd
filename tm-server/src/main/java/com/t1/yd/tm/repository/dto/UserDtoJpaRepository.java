package com.t1.yd.tm.repository.dto;

import com.t1.yd.tm.dto.model.UserDTO;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Scope("prototype")
public interface UserDtoJpaRepository extends AbstractDtoJpaRepository<UserDTO> {

    Optional<UserDTO> findByLogin(String login);

    Optional<UserDTO> findByEmail(String email);

}
