package com.t1.yd.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;

public interface IDtoProjectTaskService {

    void bindTaskToProject(@NotNull String userId, @NotNull String taskId, @NotNull String projectId);

    void removeProjectById(@NotNull String userId, @NotNull String projectId);

    void unbindTaskFromProject(@NotNull String userId, @NotNull String taskId, @NotNull String projectId);

}