package com.t1.yd.tm.service.dto;

import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.dto.IDtoService;
import com.t1.yd.tm.dto.model.AbstractEntityDTO;
import com.t1.yd.tm.exception.entity.EntityNotFoundException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.repository.dto.AbstractDtoJpaRepository;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;

import java.util.Collection;
import java.util.List;

public abstract class AbstractDtoService<E extends AbstractEntityDTO, R extends AbstractDtoJpaRepository<E>> implements IDtoService<E> {

    @NotNull
    protected final ILoggerService loggerService;

    public AbstractDtoService(@NotNull final ILoggerService loggerService) {
        this.loggerService = loggerService;
    }

    @NotNull
    protected abstract R getRepository();

    @NotNull
    @Override
    public List<E> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort);
    }

    @NotNull
    @Override
    @SneakyThrows
    public E add(@NotNull final E entity) {
        getRepository().save(entity);
        return entity;
    }

    @Override
    @SneakyThrows
    public void clear() {
        getRepository().deleteAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll() {
        return getRepository().findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        return getRepository().findById(id).orElse(null);
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        @Nullable final E result = findOneById(id);
        if (result == null) throw new EntityNotFoundException();
        getRepository().deleteById(id);
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        return getRepository().existsById(id);
    }

    @Override
    @SneakyThrows
    public long getSize() {
        return getRepository().count();
    }

    @Override
    @SneakyThrows
    public @NotNull Collection<E> set(@NotNull Collection<E> collection) {
        getRepository().deleteAll();
        getRepository().saveAll(collection);
        return collection;
    }

    @Override
    @SneakyThrows
    public @NotNull Collection<E> add(@NotNull Collection<E> collection) {
        getRepository().saveAll(collection);
        return collection;
    }

    @NotNull
    @Override
    public E update(@Nullable E entity) {
        if (entity == null) throw new EntityNotFoundException();
        getRepository().save(entity);
        return entity;
    }

}