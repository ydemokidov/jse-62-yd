package com.t1.yd.tm.listener.system;

import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    private final IPropertyService propertyService;

    public AbstractSystemListener(@NotNull final ITokenService tokenService,
                                  @NotNull final IPropertyService propertyService) {
        super(tokenService);
        this.propertyService = propertyService;
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return null;
    }
}
