package com.t1.yd.tm.dto.request.project;

import com.t1.yd.tm.dto.request.AbstractUserRequestById;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ProjectRemoveByIdRequest extends AbstractUserRequestById {

}
