package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.dto.model.ProjectDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class ProjectRemoveByIdResponse extends AbstractProjectResponse {

    public ProjectRemoveByIdResponse(@Nullable final ProjectDTO projectDTO) {
        super(projectDTO);
    }

    public ProjectRemoveByIdResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
